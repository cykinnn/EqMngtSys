﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Configuration;

namespace EqMngtSys2
{
    class SqlHelper
    {
        private static string GetConnStr()
        {
            string appPath = AppDomain.CurrentDomain.BaseDirectory;
            string dbFilePath = System.IO.Path.Combine(appPath,"db1.mdb");
            string connStr = "provider=Microsoft.Jet.OLEDB.4.0;Data Source='"+dbFilePath+"'";
            return connStr;
        }

        //private static string connStr = GetConnStr();
        private static string connStr = ConfigurationManager.ConnectionStrings["EqMngtSys2.Properties.Settings.EqInfoConnectionString"].ConnectionString;


        public static int ExecuteNonQuery(string sql, params OleDbParameter[] parameters)
        {
            
            using (OleDbConnection Aconn = new OleDbConnection(connStr))
            {
                Aconn.Open();
                using (OleDbCommand Acmd = Aconn.CreateCommand())
                {
                    Acmd.CommandText = sql;
                    Acmd.Parameters.AddRange(parameters);
                    return Acmd.ExecuteNonQuery();
                }
            }
        }

        public static object ExecuteScalar(string sql, params OleDbParameter[] parameters)
        {
            using (OleDbConnection Aconn = new OleDbConnection(connStr))
            {
                Aconn.Open();
                using (OleDbCommand Acmd = Aconn.CreateCommand())
                {
                    Acmd.CommandText = sql;
                    Acmd.Parameters.AddRange(parameters);
                    return Acmd.ExecuteScalar();
                }
            }
        }

        public static DataTable ExecuteTableQuery(string sql, params OleDbParameter[] parameters)
        {
            using (OleDbConnection Aconn = new OleDbConnection(connStr))
            {
                Aconn.Open();
                using (OleDbCommand Acmd = Aconn.CreateCommand())
                {
                    Acmd.CommandText = sql;
                    Acmd.Parameters.AddRange(parameters);
                    OleDbDataAdapter adapter = new OleDbDataAdapter(Acmd);
                    DataSet ds = new DataSet();
                    //adapter.FillSchema(ds, SchemaType.Source);
                    adapter.Fill(ds);
                    return ds.Tables[0];
                }

            }
        }

        public static object FromDbValue(object value)
        {
            if(value == DBNull.Value)
            {
                return null;
            }
            else
            {
                return value;
            }
        }

        public static object ToDbValue(object value)
        {
            if (value == null)
            {
                return DBNull.Value;
            }
            else
            {
                return value;
            }
        }
    }
}
