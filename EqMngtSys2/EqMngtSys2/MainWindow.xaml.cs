﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EqMngtSys2.DAL;
using EqMngtSys2.EqAccountUI;

namespace EqMngtSys2
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        

        private void btnShowEqAccount_Click(object sender, RoutedEventArgs e)
        {
            EqAccountListUI listUI = new EqAccountListUI();
            listUI.Show();
        }
    }
}
