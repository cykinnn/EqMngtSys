﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EqMngtSys2.Model
{
    public class t_EqAccount
    {
        public System.Int32 ID { get; set; }
        public System.String SafetyGroup { get; set; }
        public System.String EqName { get; set; }
        public System.String EqCode { get; set; }
        public System.String InvoiceEqName { get; set; }
        public System.String EqType { get; set; }
        public System.String EqClass { get; set; }
        public System.String Currency { get; set; }
        public System.String Manufacture { get; set; }
        public System.Decimal? Price { get; set; }
        public System.DateTime? PurchaseDate { get; set; }
        public System.String Position { get; set; }
        public System.DateTime? StartUsingDate { get; set; }
        public System.String Workshop { get; set; }
        public System.String CodeByFactory { get; set; }
        public System.String AssetsCode { get; set; }
        public System.Double? Power { get; set; }
        public System.String PowerUnit { get; set; }
        public System.String InvoiceNo { get; set; }
        public System.String Status { get; set; }
        public System.String Remarks { get; set; }
    }

}
