﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EqMngtSys2.Model;
using System.Data;
using System.Data.OleDb;

namespace EqMngtSys2.DAL
{
    public class EqAccountDAL
    {
        private t_EqAccount ToModel(DataRow row)
        {
            t_EqAccount model = new t_EqAccount();
            model.ID = (System.Int32)SqlHelper.FromDbValue(row["ID"]);
            model.SafetyGroup = (System.String)SqlHelper.FromDbValue(row["SafetyGroup"]);
            model.EqName = (System.String)SqlHelper.FromDbValue(row["EqName"]);
            model.EqCode = (System.String)SqlHelper.FromDbValue(row["EqCode"]);
            model.InvoiceEqName = (System.String)SqlHelper.FromDbValue(row["InvoiceEqName"]);
            model.EqType = (System.String)SqlHelper.FromDbValue(row["EqType"]);
            model.EqClass = (System.String)SqlHelper.FromDbValue(row["EqClass"]);
            model.Currency = (System.String)SqlHelper.FromDbValue(row["Currency"]);
            model.Manufacture = (System.String)SqlHelper.FromDbValue(row["Manufacture"]);
            model.Price = (System.Decimal?)SqlHelper.FromDbValue(row["Price"]);
            model.PurchaseDate = (System.DateTime?)SqlHelper.FromDbValue(row["PurchaseDate"]);
            model.Position = (System.String)SqlHelper.FromDbValue(row["Position"]);
            model.StartUsingDate = (System.DateTime?)SqlHelper.FromDbValue(row["StartUsingDate"]);
            model.Workshop = (System.String)SqlHelper.FromDbValue(row["Workshop"]);
            model.CodeByFactory = (System.String)SqlHelper.FromDbValue(row["CodeByFactory"]);
            model.AssetsCode = (System.String)SqlHelper.FromDbValue(row["AssetsCode"]);
            model.Power = (System.Double?)SqlHelper.FromDbValue(row["Power"]);
            model.PowerUnit = (System.String)SqlHelper.FromDbValue(row["PowerUnit"]);
            model.InvoiceNo = (System.String)SqlHelper.FromDbValue(row["InvoiceNo"]);
            model.Status = (System.String)SqlHelper.FromDbValue(row["Status"]);
            model.Remarks = (System.String)SqlHelper.FromDbValue(row["Remarks"]);
            return model;
        }

        public t_EqAccount[] ListAll()
        {
            DataTable table = SqlHelper.ExecuteTableQuery("SELECT [ID],[EqCode],[EqName],[InvoiceEqName],[EqType],[EqClass],[Manufacture],[Price],[Currency],[PurchaseDate],[StartUsingDate],[Workshop],[Position],[CodeByFactory],[AssetsCode],[Power],[PowerUnit],[InvoiceNo],[Status],[SafetyGroup],[Remarks] FROM t_EqAccount ORDER BY [ID]");
            t_EqAccount[] models = new t_EqAccount[table.Rows.Count];
            for (int i = 0; i < table.Rows.Count; i++)
            {
                models[i] = ToModel(table.Rows[i]);
            }
            return models;
        }

        public int Insert(t_EqAccount model)
        {
            return SqlHelper.ExecuteNonQuery("INSERT INTO t_EqAccount ([ID],[SafetyGroup],[EqName],[EqCode],[InvoiceEqName],[EqType],[EqClass],[Currency],[Manufacture],[Price],[PurchaseDate],[Position],[StartUsingDate],[Workshop],[CodeByFactory],[AssetsCode],[Power],[PowerUnit],[InvoiceNo],[Status],[Remarks]) VALUES (@ID,@SafetyGroup,@EqName,@EqCode,@InvoiceEqName,@EqType,@EqClass,@Currency,@Manufacture,@Price,@PurchaseDate,@Position,@StartUsingDate,@Workshop,@CodeByFactory,@AssetsCode,@Power,@PowerUnit,@InvoiceNo,@Status,@Remarks)",
            new OleDbParameter("@ID", SqlHelper.ToDbValue(model.ID)),
            new OleDbParameter("@SafetyGroup", SqlHelper.ToDbValue(model.SafetyGroup)),
            new OleDbParameter("@EqName", SqlHelper.ToDbValue(model.EqName)),
            new OleDbParameter("@EqCode", SqlHelper.ToDbValue(model.EqCode)),
            new OleDbParameter("@InvoiceEqName", SqlHelper.ToDbValue(model.InvoiceEqName)),
            new OleDbParameter("@EqType", SqlHelper.ToDbValue(model.EqType)),
            new OleDbParameter("@EqClass", SqlHelper.ToDbValue(model.EqClass)),
            new OleDbParameter("@Currency", SqlHelper.ToDbValue(model.Currency)),
            new OleDbParameter("@Manufacture", SqlHelper.ToDbValue(model.Manufacture)),
            new OleDbParameter("@Price", SqlHelper.ToDbValue(model.Price)),
            new OleDbParameter("@PurchaseDate", SqlHelper.ToDbValue(model.PurchaseDate)),
            new OleDbParameter("@Position", SqlHelper.ToDbValue(model.Position)),
            new OleDbParameter("@StartUsingDate", SqlHelper.ToDbValue(model.StartUsingDate)),
            new OleDbParameter("@Workshop", SqlHelper.ToDbValue(model.Workshop)),
            new OleDbParameter("@CodeByFactory", SqlHelper.ToDbValue(model.CodeByFactory)),
            new OleDbParameter("@AssetsCode", SqlHelper.ToDbValue(model.AssetsCode)),
            new OleDbParameter("@Power", SqlHelper.ToDbValue(model.Power)),
            new OleDbParameter("@PowerUnit", SqlHelper.ToDbValue(model.PowerUnit)),
            new OleDbParameter("@InvoiceNo", SqlHelper.ToDbValue(model.InvoiceNo)),
            new OleDbParameter("@Status", SqlHelper.ToDbValue(model.Status)),
            new OleDbParameter("@Remarks", SqlHelper.ToDbValue(model.Remarks)));
        }

        public int DeleteById(int Id)
        {
            return SqlHelper.ExecuteNonQuery("DELETE FROM [t_EqAccount] WHERE [Id]=@Id",
                new OleDbParameter("@Id", SqlHelper.ToDbValue(Id)));
        }

        public int Update(t_EqAccount model)
        {
            return SqlHelper.ExecuteNonQuery("UPDATE t_EqAccount SET [SafetyGroup]=@SafetyGroup,[EqName]=@EqName,[EqCode]=@EqCode,[InvoiceEqName]=@InvoiceEqName,[EqType]=@EqType,[EqClass]=@EqClass,[Currency]=@Currency,[Manufacture]=@Manufacture,[Price]=@Price,[PurchaseDate]=@PurchaseDate,[Position]=@Position,[StartUsingDate]=@StartUsingDate,[Workshop]=@Workshop,[CodeByFactory]=@CodeByFactory,[AssetsCode]=@AssetsCode,[Power]=@Power,[PowerUnit]=@PowerUnit,[InvoiceNo]=@InvoiceNo,[Status]=@Status,[Remarks]=@Remarks WHERE [Id]=@Id",
            new OleDbParameter("@SafetyGroup", SqlHelper.ToDbValue(model.SafetyGroup)),
            new OleDbParameter("@EqName", SqlHelper.ToDbValue(model.EqName)),
            new OleDbParameter("@EqCode", SqlHelper.ToDbValue(model.EqCode)),
            new OleDbParameter("@InvoiceEqName", SqlHelper.ToDbValue(model.InvoiceEqName)),
            new OleDbParameter("@EqType", SqlHelper.ToDbValue(model.EqType)),
            new OleDbParameter("@EqClass", SqlHelper.ToDbValue(model.EqClass)),
            new OleDbParameter("@Currency", SqlHelper.ToDbValue(model.Currency)),
            new OleDbParameter("@Manufacture", SqlHelper.ToDbValue(model.Manufacture)),
            new OleDbParameter("@Price", SqlHelper.ToDbValue(model.Price)),
            new OleDbParameter("@PurchaseDate", SqlHelper.ToDbValue(model.PurchaseDate)),
            new OleDbParameter("@Position", SqlHelper.ToDbValue(model.Position)),
            new OleDbParameter("@StartUsingDate", SqlHelper.ToDbValue(model.StartUsingDate)),
            new OleDbParameter("@Workshop", SqlHelper.ToDbValue(model.Workshop)),
            new OleDbParameter("@CodeByFactory", SqlHelper.ToDbValue(model.CodeByFactory)),
            new OleDbParameter("@AssetsCode", SqlHelper.ToDbValue(model.AssetsCode)),
            new OleDbParameter("@Power", SqlHelper.ToDbValue(model.Power)),
            new OleDbParameter("@PowerUnit", SqlHelper.ToDbValue(model.PowerUnit)),
            new OleDbParameter("@InvoiceNo", SqlHelper.ToDbValue(model.InvoiceNo)),
            new OleDbParameter("@Status", SqlHelper.ToDbValue(model.Status)),
            new OleDbParameter("@Remarks", SqlHelper.ToDbValue(model.Remarks)),
            new OleDbParameter("@Id", SqlHelper.ToDbValue(model.ID)));
        }

        public t_EqAccount GetById(int Id)
        {
            DataTable table = SqlHelper.ExecuteTableQuery("SELECT [ID],[SafetyGroup],[EqName],[EqCode],[InvoiceEqName],[EqType],[EqClass],[Currency],[Manufacture],[Price],[PurchaseDate],[Position],[StartUsingDate],[Workshop],[CodeByFactory],[AssetsCode],[Power],[PowerUnit],[InvoiceNo],[Status],[Remarks] FROM t_EqAccount WHERE [Id]=@Id",
                new OleDbParameter("@Id", SqlHelper.ToDbValue(Id)));
            if (table.Rows.Count <= 0)
            {
                return null;
            }
            else if (table.Rows.Count > 1)
            {
                throw new Exception("用户重复！");
            }
            else
            {
                DataRow row = table.Rows[0];
                return ToModel(row);
            }
        }

        public t_EqAccount GetByName(string Name)
        {
            DataTable table = SqlHelper.ExecuteTableQuery("SELECT [ID],[SafetyGroup],[EqName],[EqCode],[InvoiceEqName],[EqType],[EqClass],[Currency],[Manufacture],[Price],[PurchaseDate],[Position],[StartUsingDate],[Workshop],[CodeByFactory],[AssetsCode],[Power],[PowerUnit],[InvoiceNo],[Status],[Remarks] FROM t_EqAccount WHERE [Name]=@Name",
                new OleDbParameter("@Name", SqlHelper.ToDbValue(Name)));
            if (table.Rows.Count <= 0)
            {
                return null;
            }
            else if (table.Rows.Count > 1)
            {
                throw new Exception("用户重复！");
            }
            else
            {
                DataRow row = table.Rows[0];
                return ToModel(row);
            }
        }


    }
}
