﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EqMngtSys2.DAL;
using EqMngtSys2.Model;
using System.Data;
using Microsoft.Win32;
using EqMngtSys2.Helpers;

namespace EqMngtSys2.EqAccountUI
{

    /// <summary>
    /// EqAccountListUI.xaml 的交互逻辑
    /// </summary>
    public partial class EqAccountListUI : Window
    {
        private void LoadData()
        {
            dgEqAccount.ItemsSource = new EqAccountDAL().ListAll();
        }

        private void ToExcel()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel 97-2003文件(*.xls)|*.xls";
            string filename;
            if (sfd.ShowDialog() == true)
            {
                filename = sfd.FileName;

                t_EqAccount[] models = (t_EqAccount[])dgEqAccount.ItemsSource;
                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("序号"));
                table.Columns.Add(new DataColumn("设备编号"));
                table.Columns.Add(new DataColumn("设备名称"));
                table.Columns.Add(new DataColumn("发票设备编号"));
                table.Columns.Add(new DataColumn("设备型号"));
                table.Columns.Add(new DataColumn("设备类别"));
                table.Columns.Add(new DataColumn("生产厂商"));
                table.Columns.Add(new DataColumn("价格"));
                table.Columns.Add(new DataColumn("货币"));
                table.Columns.Add(new DataColumn("购买日期"));
                table.Columns.Add(new DataColumn("开始使用日期"));
                table.Columns.Add(new DataColumn("车间"));
                table.Columns.Add(new DataColumn("方位"));
                table.Columns.Add(new DataColumn("出厂编号"));
                table.Columns.Add(new DataColumn("固定资产编号"));
                table.Columns.Add(new DataColumn("功率"));
                table.Columns.Add(new DataColumn("功率单位"));
                table.Columns.Add(new DataColumn("发票号"));
                table.Columns.Add(new DataColumn("状态"));
                table.Columns.Add(new DataColumn("专业组"));
                table.Columns.Add(new DataColumn("备注"));
                foreach (t_EqAccount model in models)
                {
                    DataRow row = table.NewRow();
                    row["序号"] = model.ID;
                    row["设备编号"] = model.EqCode;
                    row["设备名称"] = model.EqName;
                    row["发票设备编号"] = model.InvoiceEqName;
                    row["设备型号"] = model.EqType;
                    row["设备类别"] = model.EqClass;
                    row["生产厂商"] = model.Manufacture;
                    row["价格"] = model.Price;
                    row["货币"] = model.Currency;
                    row["购买日期"] = model.PurchaseDate;
                    row["开始使用日期"] = model.StartUsingDate;
                    row["车间"] = model.Workshop;
                    row["方位"] = model.Position;
                    row["出厂编号"] = model.CodeByFactory;
                    row["固定资产编号"] = model.AssetsCode;
                    row["功率"] = model.Power;
                    row["功率单位"] = model.PowerUnit;
                    row["发票号"] = model.InvoiceNo;
                    row["状态"] = model.Status;
                    row["专业组"] = model.SafetyGroup;
                    row["备注"] = model.Remarks;
                    table.Rows.Add(row);
                }
                ExcelHelper.SaveToFile(ExcelHelper.RenderToExcel(table), filename);
            }

        }

        private void ModifyItem()
        {
            t_EqAccount model = (t_EqAccount)dgEqAccount.SelectedItem;
            if (model == null)
            {
                MessageBox.Show("请选择要编辑的行！");
                return;
            }
            EqAccountEditUI editUI = new EqAccountEditUI();
            editUI.IsInsert = false;
            editUI.EditingId = model.ID;
            if (editUI.ShowDialog() == true)
            {
                //LoadConditionData();
                LoadData();
            }
        }


        public EqAccountListUI()
        {
            InitializeComponent();
        }

        private void winEqAccountUI_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void btnToExcel_Click(object sender, RoutedEventArgs e)
        {
            ToExcel();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            EqAccountEditUI editUI = new EqAccountEditUI();
            editUI.IsInsert = true;
            if (editUI.ShowDialog() == true)
            {
                //LoadConditionData();
                LoadData();
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            ModifyItem();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            t_EqAccount model = (t_EqAccount)dgEqAccount.SelectedItem;
            if (model == null)
            {
                MessageBox.Show("请选择要编辑的行！");
                return;
            }
            if (MessageBox.Show("确定要删除该行吗？删除后不可恢复！", "警告！", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                new EqAccountDAL().DeleteById(model.ID);
                //LoadConditionData();
                LoadData();
            }
            
        }
    }
}
