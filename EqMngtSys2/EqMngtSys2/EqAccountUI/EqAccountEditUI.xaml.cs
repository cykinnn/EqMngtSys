﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EqMngtSys2.EqAccountUI
{
    /// <summary>
    /// EqAccountEditUI.xaml 的交互逻辑
    /// </summary>
    public partial class EqAccountEditUI : Window
    {
        public bool IsInsert { get; set; }
        public int EditingId { get; set; }


        public EqAccountEditUI()
        {
            InitializeComponent();
        }
    }
}
